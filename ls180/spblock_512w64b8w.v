// SPBlock_512W64B8W simulation mode
module spblock_512w64b8w(
    input clk,
    input [8:0] a,
    input [63:0] d,
    output [63:0] q,
    // Width of WE determines the write granularity
    input [7:0] we
);

genvar i;

reg [63:0] ram [511:0];
wire[7:0] d_split [7:0];
reg [8:0] a_hold;

always @(posedge clk) begin
    a_hold <= a;
end

assign q = ram[a_hold];

generate
    for (i = 0; i < 8; i = i + 1) begin
        assign d_split[i] = d[((i + 1)*8 - 1):i*8];

        always @(posedge clk) begin
            if (we[i]) begin
                ram[a][((i + 1)*8 - 1):i*8] = d_split[i];
            end
        end
    end
endgenerate

endmodule
