#!/usr/bin/env python3
"""converts NIOLIB and NSXLIB from VBE into VHDL
"""

import os
import sys

# use the chroot to set up
# https://git.libre-soc.org/?p=dev-env-setup.git;a=blob;f=coriolis2-chroot;hb=HEAD
# reason for using the chroot: it's standardised across the ls180 project

VASY_CMD = "schroot -c coriolis -d /tmp -- ~/alliance/install/bin/vasy"
ALLIANCEBASE = "../../alliance-check-toolkit/cells"
ALLIANCE_LIBS = ['nsxlib', 'niolib']

for libname in ALLIANCE_LIBS:

    NSXLIB = "%s/%s" % (ALLIANCEBASE, libname)

    os.system("mkdir -p %s" % libname)

    for fname in os.listdir(NSXLIB):
        if not fname.endswith(".vbe"):
            continue
        print (fname)
        prefix = fname[:-4] # strip ".vbe"
        os.system("cp %s/%s /tmp" % (NSXLIB, fname))
        os.system("rm -f /tmp/%s.vhd" % (prefix))
        os.system("%s -s -I vbe %s %s" % (VASY_CMD, fname, prefix))
        os.system("cp /tmp/%s.vhd %s" % (prefix, libname))
